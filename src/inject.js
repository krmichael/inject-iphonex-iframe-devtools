import isServer from "./isServer";

export default function injectIphoneXFrame() {
  if (!isServer) {
    iPhoneX();

    window.onresize = window.onorientationchange = function() {
      setTimeout(iPhoneX, 100);
    };

    function iPhoneX() {
      if (window.innerWidth === 375 && window.innerHeight === 812) {
        if (!document.getElementById("iphone_layout")) {
          var img = document.createElement("img");
          img.id = "iphone_layout";
          img.style.position = "fixed";
          img.style.zIndex = 9999;
          img.style.pointerEvents = "none";
          img.src = "./assets/iphonex.png";
          document.body.insertBefore(img, document.body.children[0]);
        }
      } else if (document.getElementById("iphone_layout")) {
        document.body.removeChild(document.getElementById("iphone_layout"));
      }
    }
  }
}
